xcor, a fast library for cross-correlation in Python
====================================================
Welcome, you found xcor! It makes it really fast to cross correlate a matrix.

There is currently one entry point:

`maxima, argmaxima = crossCorrelateMatrix(x, y=None, verbose=False, batch_row_size=50, compare_numpy=False)`

This takes in two matrices and cross-correlates every pair of entries (or takes in just one matrix and cross-correlates every pair of entries). Verbosity enables printing of timing information, while the batch row size parameter is for large matrices. The return value is a tuple of two matrices, the first being the maximum and the second being the relative argmaximum (how far to shift the first matrix to have it be maximally cross-correlated with the first).

This program, benchmarked on a TITAN X, generates a full 10k * 10k max cross-correlation matrix of a 10k * 25 input matrix in 8.7 seconds (block size 600), and the same for a 5k * 207 matrix in 18.9 seconds (block size 125).

Data are only copied back from the GPU for final assembly of the output array. If you want the full matrix, use `compare_numpy` and read out the last argument.

If you think this is useful or have suggestions, then feel free to email me at nasa@stanford.edu.
