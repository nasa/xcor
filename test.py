from __future__ import print_function
import xcor as xcor
import numpy as np
import sys
import os
import time

if __name__ == "__main__":
    #batch = 5000
    #N = 207
    if len(sys.argv) > 1:
        batch = int(sys.argv[1])
    else:
        batch = 107
    if len(sys.argv) > 2:
        N = int(sys.argv[2])
    else:
        N = 19
    if len(sys.argv) > 3:
        batch_size = int(sys.argv[3])
    else:
        batch_size = 100

    np.random.seed(1234)
    channels = 64
    arr = np.asarray(np.random.randn(batch, channels, N), np.float32)
    arr2 = arr[:,::-1,::-1];

    x = np.array((arr - np.mean(arr))/np.std(arr), dtype=np.float32)
    x2 = np.array((arr2-np.mean(arr2))/np.std(arr), dtype=np.float32);
    start = time.time()
    #values, indices = xcor.crossCorrelateMatrix(x, verbose=True, batch_row_size=batch_size)#, compare_numpy=True)
    values, indices, npvalues, npindices, xc = xcor.crossCorrelateMatrix(x, x2, verbose=True, batch_row_size=batch_size, compare_numpy=True)
    print("Vshape %s, ishape %s, npv shape %s, npi shape %s, xc shape %s" % (values.shape, indices.shape, npvalues.shape, npindices.shape, xc.shape))
    print("Total GPU time", time.time() - start)

    scriptsDir = os.environ.get("UTIL_SCRIPTS_DIR");
    if (scriptsDir is None):
        raise Exception("Please set environment variable UTIL_SCRIPTS_DIR");
    sys.path.insert(0,scriptsDir);
    import pathSetter;
    import util;

    count = 0;
    nr = []
    real = []
    print("Running test");
    for tuples in [(a,b) for a in xrange(batch) for b in xrange(batch)]:
        if tuples[0] <= tuples[1]:
            best, idx, smaller = util.getBestLengthwiseCrossCorrelationOfArrays(x[tuples[0]], x2[tuples[1]]
                                    , normaliseFunc=lambda x:x, normaliseByTwoNormAtEachPos=False)
            #mybest, myidx = values[tuples], indices[tuples]
            mybest, myidx, npbest, npidx, xcv = values[tuples], indices[tuples], np.real(npvalues[tuples]), np.real(npindices[tuples]), xc[tuples]

            errorMsg = "%s at %s vs %s (%s) at %s (%s) = %s\n%s\n%s\n%s" % (best, idx, mybest, npbest, myidx, npidx, tuples, x[tuples[0]][0], x[tuples[1]][0], xcv)
            if abs(npbest.imag) > 0.05 or npbest.imag != npbest.imag:
                nr.append((tuples, errorMsg))
                continue
            #errorMsg = "%s at %s vs %s at %s = %s\n%s\n%s" % (best, idx, mybest, myidx, tuples, x[tuples[0]][0], x[tuples[1]][0])
            real.append(tuples)
            #assert idx == npidx and np.allclose(best, npbest, rtol=0.05), errorMsg
            assert idx == npidx and np.allclose(best, npbest, rtol=0.0001), errorMsg
            #nonzero = np.nonzero(mybest)
            assert idx == myidx and np.allclose(best, mybest, rtol=0.0001), errorMsg
    print("nonreal value indices:", [t for t,n in nr])
    print("nonreal value count:", len(set([t for t,n in nr])))
    print("real (and correct) value count:", len(set(real)))

    #for tuples in [(a,b) for a in xrange(batch-1) for b in xrange(batch-1)]:
#    for tuples in [(a,b) for a in xrange(3) for b in xrange(3)]:
#        if tuples[1] > tuples[0]:
#            print "1/1:\n", util.crossCorrelateArraysLengthwise(x[tuples[0]], x[tuples[1]], lambda x: x)[0]
#            print "1/2:\n", util.crossCorrelateArraysLengthwise(x[tuples[0]], x[tuples[1]+1], lambda x: x)[0]
#            print "2/1:\n", util.crossCorrelateArraysLengthwise(x[tuples[0]+1], x[tuples[1]], lambda x: x)[0]
#            print "2/2:\n", util.crossCorrelateArraysLengthwise(x[tuples[0]+1], x[tuples[1]+1], lambda x: x)[0]
#            print "1/1+2/2:\n", util.crossCorrelateArraysLengthwise(x[tuples[0]+1], x[tuples[1]+1], lambda x: x)[0] + util.crossCorrelateArraysLengthwise(x[tuples[0]], x[tuples[1]], lambda x: x)[0]
#            print "Pair:\n", util.crossCorrelateArraysLengthwise(x[tuples[0]:tuples[0]+2], x[tuples[1]], lambda x: x)[0]
