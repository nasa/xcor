from __future__ import print_function
import numpy as np
import sys
import random
import pycuda.autoinit
import pycuda.gpuarray
import pycuda.driver
import pycuda.compiler

def count_mismatches(x, y):
    ax = np.array([c for c in x.lower()])
    ay = np.array([c for c in y.lower()])
    return sum(ax != ay)

def mismax(seqlets, mismatches=1):
    paired = [[] for i in range(len(seqlets))]
    for ix, x in enumerate(seqlets):
        for iy, y in enumerate(seqlets):
            if count_mismatches(x, y) <= mismatches:
                paired[ix].append(iy)

    return paired

def random_seqlets(n = 1024, k=7):
    s = []
    for i in range(n):
        s.append(random_seqlet(k))
    return s

def random_seqlet(k = 7):
    return "".join([random.choice("ATCG") for i in range(k)])

def seqlet2int(seqlet):
    base2int = {"A": 1, "C": 2, "G": 4, "T": 8}
    rev2int = {"A": 8, "C": 4, "G": 2, "T": 1}
    fwd = 0
    rev = 0
    for i, base in enumerate(seqlet):
        fwd += base2int[base] << (4*i)
        rev += rev2int[base] << (4*(len(seqlet) - i - 1))
    return fwd
    if rev < fwd:
        return rev
    else:
        return fwd

def seqlets2mat(seqlets):
    return np.array(map(seqlet2int, seqlets), np.uint32)

def gpumismax(seqlets, mismatches=1):
    mismax_kernel = """
    __global__ void MismaxKernel(int *globalInput, int *globalOutput, int num_seqlets, int offset, int mismatch_threshold)
    {
        extern __shared__ int fixed_data[];
        int index_x = blockIdx.x * blockDim.x + threadIdx.x + offset;

        if (threadIdx.y == 0)
            fixed_data[threadIdx.x] = (index_x < num_seqlets)?globalInput[index_x]:0;

        __syncthreads();

        for (int ty = threadIdx.y; ty < num_seqlets; ty += blockDim.y) {
            int mismatches = __popc(fixed_data[threadIdx.x] & globalInput[ty]) >= mismatch_threshold;
            globalOutput[blockIdx.x * blockDim.x + threadIdx.x + blockDim.x * gridDim.x * ty] = mismatches;
        }
    }
    """
    gmem = pycuda.gpuarray.to_gpu(seqlets2mat(seqlets))
    mod = pycuda.compiler.SourceModule(mismax_kernel)
    mismax_kernel = mod.get_function("MismaxKernel")
    x_threads = 32
    y_threads = 8
    mismax_gpu = pycuda.gpuarray.empty((len(seqlets), len(seqlets)), np.uint32)
    mismax_kernel(gmem, mismax_gpu, np.int32(len(seqlets)), np.int32(0), np.int32(len(seqlets[0]) - mismatches), block=(x_threads, y_threads, 1), grid=(len(seqlets)/x_threads, 1), shared=x_threads * np.dtype('uint32').itemsize)
    return mismax_gpu.get()

def find_hits(chunk, offset):
    chunk_hits = []
    index = 0
    for bit in range(32):
        for byte in range(len(chunk)):
            if chunk[byte] & (1 << bit):
                chunk_hits.append(offset + index)
            index += 1
    return chunk_hits

def row2list(indexed_row, y_threads = 2):
    """Convert a row of gpu output to a list of hit indices.
        The first hit is in bit 0 of the first integer, the second hit is in bit 0 of the second integer, ... up to y_threads,
        then the (y_threads + 1)st hit is in bit 1 of the first integer, etc."""
    index, row = indexed_row
    return sum(map(list, np.where(row)), [])

def gpu2tuple(gpu):
    """Convert the matrix from the GPU to a tuple representation compatible with the CPU.
        Each row of the matrix is independent, so row2list handles the nasty stuff.
        """
    return map(row2list, enumerate(gpu))

def compare_mismax(seqlets):
    import time
    start = time.time()
    cpu = mismax(seqlets)
    cpu_time = time.time()
    print("CPU time:", cpu_time - start)
    gpu = gpumismax(seqlets)
    gpu_time = time.time()
    print("GPU time:", gpu_time - cpu_time)
    gputuple = gpu2tuple(gpu)
    print("GPU convert time:", time.time() - gpu_time)
    for compare in zip(cpu, gputuple):
        assert compare[0] == compare[1]

if __name__ == "__main__":
    #print(gpumismax(random_seqlets(n=int(sys.argv[1]) if len(sys.argv) > 1 else 1024, k=int(sys.argv[2]) if len(sys.argv) > 2 else 7)).shape)
    compare_mismax(random_seqlets(n=int(sys.argv[1]) if len(sys.argv) > 1 else 64, k=int(sys.argv[2]) if len(sys.argv) > 2 else 7))
