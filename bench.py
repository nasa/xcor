from __future__ import print_function
import xcor as xcor
import numpy as np
import sys
import os
import time

if __name__ == "__main__":
    if len(sys.argv) > 1:
        batch = int(sys.argv[1])
    else:
        batch = 2000
    if len(sys.argv) > 2:
        N = int(sys.argv[2])
    else:
        N = 300
    if len(sys.argv) > 3:
        channels = int(sys.argv[2])
    else:
        channels = 60
    if len(sys.argv) > 4:
        batch_size = int(sys.argv[3])
    else:
        batch_size = 200

    arr = np.asarray(np.random.randn(batch,channels,N), np.float32)

    x = np.array((arr - np.mean(arr))/np.std(arr), dtype=np.float32)
    arry = np.asarray(np.random.randn(batch,channels,N), np.float32)

    y = np.array((arry - np.mean(arry))/np.std(arry), dtype=np.float32)
    start = time.time()
    values, indices = xcor.crossCorrelateMatrix(x, y, verbose=True, batch_row_size=batch_size)
    print("Total GPU time", time.time() - start)
