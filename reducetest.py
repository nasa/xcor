import pycuda.autoinit
import pycuda.gpuarray as gpuarray
import numpy
import axisreduction

signals = 4

array_shape = (4000, 50, 25)
axis = 1

# Split into:
# 1) dimensions that are outside the range being summed
# 2) dimension that is being summed
# 3) dimensions that are inside the range being summed
# Because sums are commutative, we can flatten (1) and (3).
outside = numpy.prod(array_shape[:axis])
reduced = array_shape[axis]
inside = numpy.prod(array_shape[axis+1:])

krnl = axisreduction.AxisReductionKernel(numpy.float32, neutral="0",
        reduce_expr="a + b", arguments="float *in")

rand = numpy.array(numpy.random.randn(500), dtype=numpy.float32)

grand = gpuarray.to_gpu(rand)

out = krnl(grand).get()

print(out)
print(numpy.sum(rand))

rand = numpy.array(numpy.random.randn(500, 5, 10), dtype=numpy.float32)

grand = gpuarray.to_gpu(rand)

out = krnl(grand, axis=-1).get()
cpu = numpy.sum(rand, axis=-1)

print(out.shape)
print(cpu.shape)
print(numpy.allclose(out, cpu, atol=1e-4))

out = krnl(grand, axis=1).get()
cpu = numpy.sum(rand, axis=1)

print(out.shape)
print(cpu.shape)
print(numpy.allclose(out, cpu, atol=1e-4))

out = krnl(grand, axis=0).get()
cpu = numpy.sum(rand, axis=0)

print(out.shape)
print(cpu.shape)
print(numpy.allclose(out, cpu, atol=1e-4))
