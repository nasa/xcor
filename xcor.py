from __future__ import print_function
import pycuda.gpuarray
import pycuda.driver
import pycuda.compiler
import numpy as np
import time
import os
import sys
import pyfft.cuda

def run_max_and_index(xc_gpu, xm_gpu, xi_gpu, vector_count, vector_count2, fft_length, verbose, vector_length, vector_length2, batch_row_size=None, context=None):
    if context is None:
        import pycuda.autoinit
    max_and_index_kernel = """
    #include <float.h>
    __device__ __constant__ int vector_count;
    __device__ __constant__ int vector_count2;
    __device__ __constant__ int vector_length;
    __device__ __constant__ int max_index;
    __device__ __constant__ int index_offset;
    #define tz (threadIdx.z)
    __global__ void MaxAndIndexKernel(float *array, float *maxima, int *argmaxima)
    {
        int tx = threadIdx.x + blockIdx.x * blockDim.x;
        int ty = threadIdx.y + blockIdx.y * blockDim.y;
        int block_offset = (ty * vector_count2 + tx) * vector_length;

        if (tx > vector_count2 || ty > vector_count) return;

        __shared__ float maximum[%(ZTHREADS)s];
        __shared__ int index[%(ZTHREADS)s];
        maximum[tz] = -FLT_MAX;
        index[tz] = -1;
    
        for (int idz = threadIdx.z; idz < max_index; idz+=%(ZTHREADS)s) {
            // complex numbers were transparently converted to float32 by reduce kernel
            float value = array[(block_offset + idz)*2];
            if (value > maximum[tz])
            {
                maximum[tz] = value;
                index[tz] = idz;
            }
        }

        for (int maxthread = %(ZTHREADS)s >> 1; maxthread > 0; maxthread >>= 1)
        {
            __syncthreads();
            if (tz < maxthread) {
                if (maximum[tz + maxthread] > maximum[tz])
                {
                    maximum[tz] = maximum[tz + maxthread];
                    index[tz] = index[tz + maxthread];
                }
            }
        }
        __syncthreads();

        if (tz == 0) {
            maxima[ty * vector_count2 + tx] = maximum[0];
            argmaxima[ty * vector_count2 + tx] = index[0] - index_offset;
        }
    }
    """

    max_and_index_setup_start = time.time()

    z_threads = 64

    import pycuda.compiler
    mod = pycuda.compiler.SourceModule(max_and_index_kernel % {'ZTHREADS': z_threads}, no_extern_c=False)
    for k, v in { 'vector_count': vector_count, 'vector_count2': vector_count2, 'vector_length': fft_length, 'max_index': fft_length, 'index_offset': (vector_length - 1) + (vector_length2 - 1)}.iteritems():
        pycuda.driver.memcpy_htod(mod.get_global(k)[0], np.array(v, np.int32))

    max_and_index = mod.get_function("MaxAndIndexKernel")

    if batch_row_size is None:
        if xm_gpu is None:
            xm_gpu = pycuda.gpuarray.empty((vector_count, vector_count2), np.float32)
        if xi_gpu is None:
            xi_gpu = pycuda.gpuarray.empty((vector_count, vector_count2), np.int32)
        if verbose:
            print("MAI setup time:", time.time() - max_and_index_setup_start)
    
        import math
        max_and_index_start = time.time()
        #print(xc_gpu.shape, xm_gpu.shape, xi_gpu.shape, (8, 8, z_threads), (int(math.ceil(vector_count/8.0)),int(math.ceil(vector_count/8.0))))
        max_and_index(xc_gpu, xm_gpu, xi_gpu, block = (1, 1, z_threads), grid=(vector_count2, vector_count))
        #max_and_index(xc_gpu, xm_gpu, xi_gpu, block = (8, 8, z_threads), grid=(int(math.ceil(vector_count/8.0)),int(math.ceil(vector_count/8.0))))
    
        if verbose:
            print("MAI time:", time.time() - max_and_index_start)
    
        return xm_gpu, xi_gpu
    else:
        if xm_gpu is None:
            xm_gpu = pycuda.gpuarray.empty((batch_row_size, vector_count2), np.float32)
        if xi_gpu is None:
            xi_gpu = pycuda.gpuarray.empty((batch_row_size, vector_count2), np.int32)
        if verbose:
            print("MAI setup time:", time.time() - max_and_index_setup_start)
    
        import math
        max_and_index_start = time.time()
        max_and_index(xc_gpu, xm_gpu, xi_gpu, block = (1, 1, z_threads), grid=(vector_count2, batch_row_size))
        #max_and_index(xc_gpu, xm_gpu, xi_gpu, block = (8, 8, z_threads), grid=(int(math.ceil(vector_count/8.0)), int(math.ceil(batch_row_size/8.0))))
    
        if verbose:
            print("MAI time:", time.time() - max_and_index_start)
    
        return xm_gpu, xi_gpu

def run_ewpmk(xrevfft_gpu, xfft_gpu, vector_count, vector_count2, channel_count, fft_length, verbose=False, batch_row_size=None, batch_offset=None, context=None, xfc_gpu=None):
    if context is None:
        import pycuda.autoinit
    ewpmk_kernel = """
    __device__ __constant__ int batch_offset;
    __device__ __constant__ int vector_count;
    __device__ __constant__ int vector_count2;
    __device__ __constant__ int channel_count;
    __device__ __constant__ int vector_depth;
    __device__ __constant__ int kept_depth;

    __global__ void ElementwisePairedMultiplyKernel(float *reverse, float *forward, float *convolution)
    {
        int index_x = blockIdx.x;
        int index_y = blockIdx.y;
        extern __shared__ float smem[];

        float *sumR = smem;
        float *sumC = smem + blockDim.x * blockDim.y;

        for (int index_z = threadIdx.y; index_z < kept_depth; index_z+=blockDim.y) {
            sumR[threadIdx.y * blockDim.x + threadIdx.x] = 0;
            sumC[threadIdx.y * blockDim.x + threadIdx.x] = 0;
            __syncthreads();

            for (int index_channel = threadIdx.x; index_channel < channel_count; index_channel += blockDim.x) {
	            float reverseR = reverse[((index_x * channel_count + index_channel) * vector_depth + index_z)*2];
	            float reverseC = reverse[((index_x * channel_count + index_channel) * vector_depth + index_z)*2 + 1];
	            float forwardR = forward[(((index_y + batch_offset) * channel_count + index_channel) * vector_depth + index_z)*2];
	            float forwardC = forward[(((index_y + batch_offset) * channel_count + index_channel) * vector_depth + index_z)*2 + 1];
	            sumR[threadIdx.y * blockDim.x + threadIdx.x] += reverseR * forwardR - reverseC * forwardC;
	            sumC[threadIdx.y * blockDim.x + threadIdx.x] += reverseR * forwardC + forwardR * reverseC;
            }

            for (int maxthread = blockDim.x >> 1; maxthread > 0; maxthread >>= 1)
            {
                 __syncthreads();
                 if (threadIdx.x < maxthread) {
                    sumR[threadIdx.y*blockDim.x + threadIdx.x] += sumR[threadIdx.y * blockDim.x + threadIdx.x + maxthread];
                    sumC[threadIdx.y*blockDim.x + threadIdx.x] += sumC[threadIdx.y * blockDim.x + threadIdx.x + maxthread];
                 }
            }

            __syncthreads();

            if (threadIdx.x == 0) {
                convolution[((index_y * vector_count2 + index_x)*kept_depth + index_z)*2] = sumR[threadIdx.y * blockDim.x];
                convolution[((index_y * vector_count2 + index_x)*kept_depth + index_z)*2+1] = sumC[threadIdx.y * blockDim.x];
            }
        }
    }
    """
    
    ewpmk_setup_start = time.time()
    import pycuda.compiler
    mod = pycuda.compiler.SourceModule(ewpmk_kernel, no_extern_c=False)

    for k, v in { 'vector_count': vector_count, 'vector_count2': vector_count2, 'vector_depth': fft_length, 'kept_depth': fft_length, 'batch_offset': batch_offset, 'channel_count': channel_count }.items():
        pycuda.driver.memcpy_htod(mod.get_global(k)[0], np.array(v, np.int32))

    ewpmk = mod.get_function("ElementwisePairedMultiplyKernel")
    if verbose:
        print("EWPMK setup time:", time.time() - ewpmk_setup_start)

    # This can be optimized by having different block and grid sizes; this was kinda arbitrary.
    # Also might make sense to not have these be constants and instead be accessed, in the case
    # where you want to compile once (lots of different sized data).
    import math
    ewpmk_start = time.time()

    y_threads = 256
    x_threads = channel_count

    # These two loops set the y threads and x threads to the correct power of two
    # 1024 is the max thread count for a block in CUDA

    while y_threads > fft_length:
        y_threads /= 2
    while y_threads * x_threads > 1024:
        x_threads /= 2

    if verbose:
        print("block %s and grid %s, shm %d" % ((x_threads, y_threads, 1), (vector_count2, batch_row_size), 2*x_threads*y_threads*np.dtype('float32').itemsize))

    if batch_row_size is not None:
        if xfc_gpu is None:
            xfc_gpu = pycuda.gpuarray.empty((batch_row_size, vector_count2, fft_length, 2), np.float32)
        ewpmk(xrevfft_gpu, xfft_gpu, xfc_gpu, block = (x_threads, y_threads, 1), grid=(vector_count2, batch_row_size), shared=2*x_threads*y_threads*np.dtype('float32').itemsize)
    else:
        if xfc_gpu is None:
            xfc_gpu = pycuda.gpuarray.empty((vector_count, vector_count2, x_threads, fft_length, 2), np.float32)
        ewpmk(xrevfft_gpu, xfft_gpu, xfc_gpu, block = (x_threads, y_threads, 1), grid=(vector_count2, vector_count), shared=2*x_threads*y_threads*np.dtype('float32').itemsize)

    if verbose:
        print("EWPMK time:", time.time() - ewpmk_start)
    return xfc_gpu

def crossCorrelateMatrix(x, y=None, verbose=False, batch_row_size=50, compare_numpy=False, cuda_context=None):
    if y is None:
        y = x
    if cuda_context is None:
        import pycuda.autoinit
        cuda_context = pycuda.autoinit.context
    elif isistance(cuda_context, int):
        import pycuda.driver
        pycuda.driver.init()
        cuda_context = pycuda.driver.Device(cuda_context).make_context()
    elif isinstance(cuda_context, pycuda.driver.Context):
        pass
    input_shape = x.shape
    if len(input_shape) == 2:
        input_shape = [input_shape[0], 1, input_shape[1]]
    elif len(input_shape) == 1:
        input_shape = [1, 1, input_shape[0]]

    input_shape2 = y.shape
    if len(input_shape2) == 2:
        input_shape2 = [input_shape2[0], 1, input_shape2[1]]
    elif len(input_shape) == 1:
        input_shape2 = [1, 1, input_shape2[0]]

    if input_shape2[2] > input_shape[2]:
        raise LogicError("Please pass longer array as first argument!")

    if input_shape2[1] != input_shape[1]:
        raise LogicError("Only able to run on the same number of channels for now!")

    vector_count = input_shape[0]
    vector_count2 = input_shape2[0]
    channel_count = input_shape[1]
    vector_length = input_shape[2]
    vector_length2 = input_shape2[2]

    x = x.reshape(tuple(input_shape))
    y = y.reshape(tuple(input_shape2))

    import math
    fft_length = int(2**(math.ceil(math.log(vector_length)/math.log(2))+2))

    if verbose:
        print("FFT size", fft_length)

    xrev = np.dstack((y[:, :, ::-1], np.zeros((vector_count2, channel_count, fft_length-input_shape2[2]))))
    x = np.pad(x, pad_width=[(0, 0), (0, 0), [vector_length-1]*2], mode='constant')
    x = np.dstack((x, np.zeros((vector_count, channel_count, fft_length-x.shape[2]))))

    mkplan_start = time.time()
    xfft_gpu = pycuda.gpuarray.to_gpu(np.array(x, dtype=np.complex64))
    plan = pyfft.cuda.Plan((fft_length,), context=cuda_context)
    plan.execute(xfft_gpu, batch=vector_count * channel_count)

    xrevfft_gpu = pycuda.gpuarray.to_gpu(np.array(xrev, dtype=np.complex64))
    plan.execute(xrevfft_gpu, batch=vector_count2 * channel_count)
        
    if verbose:
        print("FFT time:", time.time() - mkplan_start)

    # now we have the 1D frequencies of each of the batch vectors of length N.
    # All that we need now is a simple elementwise multiplication. It's a little
    # more complicated because the datatype is complex but that's basically it.
    # this more or less involves replicating batch values by row in one array and
    # by column in the other and then doing a full elementwise multiply.
    
    # This is the memory limiting phase of the run. So in order to not have that
    # be a concern, particularly on older cards with less memory, use batching.
    # The most intuitive batching is to just batch by "batch", so that each row
    # in the matrix is calculated separately. If you don't have enough memory for
    # that then I'm not sure how you did the initial FFT in the first place.
    if batch_row_size is not None:
        mem_time = time.time()
        indices = np.empty((vector_count, vector_count2), np.int32)
        values = np.empty((vector_count, vector_count2), np.float32)
        if compare_numpy:
            npindices = np.empty((vector_count, vector_count2), np.int32)
            npvalues = np.empty((vector_count, vector_count2), np.complex64)
            xc = np.empty((vector_count, vector_count2, fft_length), np.complex64)
        ewpmk_time = 0
        ifft_time = 0
        max_and_index_time = 0
        xm_gpu = None
        xi_gpu = None
        xfc_gpu = None
        for batch_index in range(int(math.ceil(float(vector_count)/batch_row_size))):
            batch_offset = batch_row_size * batch_index
            # This is necessary to deal with it not necessarily evenly dividing
            batch_size = min(batch_row_size, vector_count - batch_offset)
            ewpmk_start = time.time()
            xfc_gpu = run_ewpmk(xrevfft_gpu, xfft_gpu, vector_count, vector_count2, channel_count, fft_length, False, batch_size, batch_offset, context=cuda_context, xfc_gpu=xfc_gpu)
            ewpmk_time += time.time() - ewpmk_start
            pifft_start = time.time()
            pyfft.cuda.Plan((fft_length,), normalize=True, context=cuda_context).execute(xfc_gpu, batch=vector_count2 * batch_size, inverse=True)
            ifft_time += time.time() - pifft_start
            xc_gpu = xfc_gpu
            
            # Then just run a simple reducer that finds the max per batch pair once it
            # has been brought back from frequency space. Also provide shift offsets.
    
            max_and_index_start = time.time()
            xm_gpu, xi_gpu = run_max_and_index(xc_gpu, xm_gpu, xi_gpu, vector_count, vector_count2, fft_length, False, vector_length, vector_length2, batch_row_size=batch_size, context=cuda_context)
            xi = xi_gpu.get()[:batch_size]
            xm = xm_gpu.get()[:batch_size]
            indices[batch_offset:batch_offset+batch_size] = xi
            values[batch_offset:batch_offset+batch_size] = xm
            max_and_index_time += time.time() - max_and_index_start

            if compare_numpy:
                batch_xfc = xfc_gpu.get().view(np.complex64).reshape(list(list(xfc_gpu.shape)[:-1]))[:batch_size]
                batch_trueindices = np.argmax(batch_xfc, axis=2)
                batch_npindices = batch_trueindices - ((vector_length2 - 1) + (vector_length - 1))
                batch_npvalues = np.max(batch_xfc, axis=2)

                npindices[batch_offset:batch_offset+batch_size] = batch_npindices
                npvalues[batch_offset:batch_offset+batch_size] = batch_npvalues
                xc[batch_offset:batch_offset+batch_size] = batch_xfc
        if verbose:
            print("EWPMK time:", ewpmk_time)
            print("ifft time:", ifft_time)
            print("MAI time:", max_and_index_time)
    else:
        xfc_gpu = run_ewpmk(xrevfft_gpu, xfft_gpu, vector_count, vector_count2, channel_count, fft_length, verbose, None, 0, context=cuda_context)
    
        pifft_start = time.time()
    
        pyfft.cuda.Plan((fft_length,), normalize=True, context=cuda_context).execute(xfc_gpu, batch=vector_count * vector_count2, inverse=True)
    
        if verbose:
            print("ifft time:", time.time() - pifft_start)

        xc_gpu = xfc_gpu

        if compare_numpy and verbose:
            xfc = np.real(xfc_gpu.get().view(np.complex64))
            real_sum = np.sum(xfc, axis=2)
            xc = np.real(xc_gpu.get().view(np.complex64))
            #np.testing.assert_allclose(np.squeeze(xfc), np.squeeze(xc), atol=1e-5)
            #np.testing.assert_allclose(np.squeeze(real_sum), np.squeeze(xc), atol=1e-5)

        # Then just run a simple reducer that finds the max per batch pair once it
        # has been brought back from frequency space. Also provide shift offsets.
    
        xm_gpu, xi_gpu = run_max_and_index(xc_gpu, None, None, vector_count, vector_count2, fft_length, verbose, vector_length, vector_length2, batch_row_size=batch_row_size, context=cuda_context)
    
        indices = xi_gpu.get()
        values = xm_gpu.get()

        if compare_numpy:
            xc = xc_gpu.get().view(np.complex64).reshape(list(list(xc_gpu.shape)[:-1]))
            trueindices = np.argmax(xc, axis=2)
            npindices = trueindices - ((vector_length2 - 1) + (vector_length - 1))
            npvalues = np.max(xc, axis=2)

    if compare_numpy:
        return values, indices, npvalues, npindices, xc
    else:
        return values, indices
